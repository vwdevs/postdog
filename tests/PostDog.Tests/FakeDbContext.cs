using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using PostDog.DataSource;

namespace PostDog.Tests
{
    /// <summary>
    ///     Using the fake db Context from the EF-Core documentation
    ///     https://docs.microsoft.com/en-us/ef/core/miscellaneous/testing/sqlite
    /// </summary>
    public class FakeDbContext : GenericContext
    {
        private readonly DbConnection _connection;

        public FakeDbContext() : base(
            new DbContextOptionsBuilder<GenericContext>()
                .UseSqlite(CreateInMemoryDatabase())
                .Options)
        {
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}
