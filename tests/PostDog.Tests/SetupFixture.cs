using System;
using System.Collections.Generic;
using NUnit.Framework;
using PostDog.DomainModels.Entities;

namespace PostDog.Tests
{
    [SetUpFixture]
    public class SetupFixture
    {
        public static readonly Func<FakeDbContext> SetupContext = CreateLocalDbContext;

        private static FakeDbContext CreateLocalDbContext()
        {
            var context = new FakeDbContext();
            context.Database.EnsureCreated();
            SeedDatabase(context);
            return context;
        }

        private static void SeedDatabase(FakeDbContext context)
        {
            context.Documents.Add(new Document
            {
                Id = 1,
                Description = "a",
                Sender = "a",
                Slug = "asdf",
                CreatedDate = DateTime.Now,
                DocumentGuid = Guid.NewGuid(),
                DocumentTags = new List<DocumentTag>(),
                SourceFiles = new List<SourceFile>(),
                VirtualPath = "path"
            });

            context.Documents.Add(new Document
            {
                Id = 2,
                Description = "a",
                Sender = "a",
                Slug = "asdf",
                CreatedDate = DateTime.Now,
                DocumentGuid = Guid.NewGuid(),
                DocumentTags = new List<DocumentTag>(),
                SourceFiles = new List<SourceFile>(),
                VirtualPath = "path"
            });

            context.SaveChanges();
        }
    }
}
