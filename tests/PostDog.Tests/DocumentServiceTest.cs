using System.Collections;
using System.Linq;
using NUnit.Framework;
using PostDog.Services.Services;

namespace PostDog.Tests
{
    public class DocumentServiceTest
    {
        private FakeDbContext _context;
        private DocumentService _sut;

        [SetUp]
        public void Setup()
        {
            _context = SetupFixture.SetupContext();
            _sut = new DocumentService(_context);
        }

        [Test]
        public void GetPaginatedDocumentList()
        {
            // Arrange
            // Act
            var result = _sut.GetPaginatedDocuments(0, 25);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotEmpty((IEnumerable) result.Data);
            Assert.AreEqual(1, result.PageCount);
            Assert.AreEqual("a", result.Data.First().Description);
        }
    }
}
