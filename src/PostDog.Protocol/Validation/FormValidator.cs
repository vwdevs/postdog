using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PostDog.Protocol.Validation
{
    public class FormValidator
    {
        public FormValidator()
        {
            _ruleBuilders = new Dictionary<string, RuleBuilder>();
        }
        
        private Dictionary<string, RuleBuilder> _ruleBuilders;
        public RuleBuilder For(string propertyName)
        {
            var ruleBuilder = new RuleBuilder(propertyName);
            if (_ruleBuilders.ContainsKey(propertyName))
            {
                return _ruleBuilders[propertyName];
            }
            
            _ruleBuilders.Add(propertyName, ruleBuilder);
            return ruleBuilder;
        }

        public IEnumerable<ValidationResult> ValidationResults
        {
            get
            {
                return _ruleBuilders.SelectMany(entry => entry.Value.ValidationResults);
            }
        }
    }
}