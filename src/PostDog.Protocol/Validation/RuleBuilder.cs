using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PostDog.Protocol.Validation
{
    public sealed class RuleBuilder
    {
        private readonly string _propertyName;

        public RuleBuilder(string propertyName)
        {
            _propertyName = propertyName;
            ValidationResults = new List<ValidationResult>();
        }

        private bool _hasNotEmptyFlag;
        private bool _hasSameAsFlag;
        private string _compareValue;
        public ICollection<ValidationResult> ValidationResults { get; set; }

        public RuleBuilder NotEmpty()
        {
            _hasNotEmptyFlag = true;
            return this;
        }

        public void Verify(string value)
        {
            if (_hasNotEmptyFlag)
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    ValidationResults.Add(new ValidationResult(
                        "Error, the value may not be empty or null",
                        new[] {_propertyName}));
                    return;
                }
            }

            if (_hasSameAsFlag)
            {
                if (!value.Equals(_compareValue, StringComparison.InvariantCultureIgnoreCase))
                {
                    ValidationResults.Add(new ValidationResult(
                        "Error, given value does not correspond to comparing value",
                        new[] {_propertyName}));
                    return;
                }
            }
        }

        public RuleBuilder SameAs(string compareValue)
        {
            _hasSameAsFlag = true;
            _compareValue = compareValue;
            return this;
        }
    }
}
