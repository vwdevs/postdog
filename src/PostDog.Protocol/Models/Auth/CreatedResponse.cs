using System.Net;

namespace PostDog.Protocol.Models.Auth
{
    public class CreatedResponse : BaseMessageResponse
    {
        public CreatedResponse(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}