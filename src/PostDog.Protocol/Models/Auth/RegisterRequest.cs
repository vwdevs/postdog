using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PostDog.Protocol.Models.Auth
{
    public class RegisterRequest : IValidatableObject
    {
        [Required] public string Username { get; set; }

        [Required] public string Password { get; set; }

        [Required] public string PasswordRepeat { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Password != PasswordRepeat)
                yield return new ValidationResult("Passwords do not match", new[] {nameof(Password)});
        }
    }
}