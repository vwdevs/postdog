using System.Net;

namespace PostDog.Protocol.Models.Auth
{
    public class AuthenticateResponse : BaseMessageResponse
    {
        public AuthenticateResponse(string userName, string token, string message, HttpStatusCode statusCode) : base(
            message, statusCode)
        {
            Username = userName;
            Token = token;
        }

        public string Username { get; set; }
        public string Token { get; set; }
    }
}