namespace PostDog.Protocol.Models.Documents
{
    public class DocumentRequest
    {
        public int CurrentPage { get; set; } = 1;
        public int SkipPages => (CurrentPage - 1) * PageOffset;
        public int PageCount { get; set; }
        public int PageOffset { get; set; } = 25;
    }
}