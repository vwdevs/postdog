using System;

namespace PostDog.Protocol.Models.Documents
{
    public class DocumentResult
    {
        public string Sender { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string VirtualPath { get; set; }
    }
}