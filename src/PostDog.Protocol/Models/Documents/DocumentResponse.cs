using System.Collections.Generic;

namespace PostDog.Protocol.Models.Documents
{
    public class DocumentResponse
    {
        public IEnumerable<DocumentResult> Data { get; set; }
        public int PageCount { get; set; }
    }
}