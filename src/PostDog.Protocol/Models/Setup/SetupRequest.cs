using System;
using System.Text;
using PostDog.Common.Globals;

namespace PostDog.Protocol.Models.Setup
{
    public class SetupRequest
    {
        public string DatabaseProvider { get; set; }

        public DatabaseType DatabaseType
        {
            get
            {
                if (Enum.TryParse(DatabaseProvider, out DatabaseType parsedResult))
                {
                    return parsedResult;
                }

                return DatabaseType.Undefined;
            }
        }

        public string DatabaseHost { get; set; }
        public string DatabasePort { get; set; }
        public string DatabaseUser { get; set; }
        public string DatabaseUserPassword { get; set; }
        public string DatabaseName { get; set; }

        public string ConnectionString
        {
            get
            {
                var connectionBuilder = new StringBuilder();
                switch (DatabaseType)
                {
                    case DatabaseType.MySql:
                        connectionBuilder.Append($"Server={DatabaseHost}; ")
                                         .Append($"Port={DatabasePort}; ")
                                         .Append($"Database={DatabaseName}; ")
                                         .Append($"Uid={DatabaseUser}; ")
                                         .Append($"Pwd={DatabaseUserPassword};");
                        break;
                            
                    case DatabaseType.Sqlite:
                        connectionBuilder.Append("Data Source=appdata/postdog.db");
                        break;
                    
                    case DatabaseType.PostgreSql:
                        connectionBuilder.Append($"Server={DatabaseHost};")
                                         .Append($"Port={DatabasePort};")
                                         .Append($"Database={DatabaseName};")
                                         .Append($"User Id={DatabaseUser};")
                                         .Append($"Password={DatabaseUserPassword};");
                        break;
                    
                    case DatabaseType.SqlServer:
                        connectionBuilder.Append($"Server={DatabaseHost},")
                                         .Append($"{DatabasePort};")
                                         .Append($"Database={DatabaseName};")
                                         .Append($"User Id={DatabaseUser};")
                                         .Append($"Password={DatabaseUserPassword};");
                        break;
                    
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return connectionBuilder.ToString();
            }
        }

        
        
        public string InitialUsername { get; set; }
        public string UserPassword { get; set; }
    }
}