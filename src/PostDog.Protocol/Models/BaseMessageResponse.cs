using System.Net;

namespace PostDog.Protocol.Models
{
    public abstract class BaseMessageResponse
    {
        public BaseMessageResponse(string message, HttpStatusCode statusCode)
        {
            Message = message;
            Status = statusCode;
        }

        public string Message { get; set; }
        public HttpStatusCode Status { get; set; }
    }
}