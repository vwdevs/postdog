using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PostDog.DataSource.EntityConfigurations;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource
{
    public abstract class GenericContext :
        IdentityDbContext<ApplicationUser, IdentityRole<int>, int>, IGenericContext
    {
        protected GenericContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Tag> Tags { get; set; }
        public DbSet<SourceFile> SourceFiles { get; set; }
        public DbSet<FileConversion> FileConversions { get; set; }
        public DbSet<PageFile> PageFiles { get; set; }
        public DbSet<DocumentTag> DocumentTags { get; set; }
        public DbSet<Document> Documents { get; set; }

        public virtual async Task RunMigrationsAsync(CancellationToken cancellationToken)
        {
            await Database.MigrateAsync(cancellationToken);
        }

        public Task<int> SaveChangesAsync()
        {
            return SaveChangesAsync(default);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyEntityTypeConfiguration();
            modelBuilder.ApplyIdentityModelCreating();
        }
    }
}