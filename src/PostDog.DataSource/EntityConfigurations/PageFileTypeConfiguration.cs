using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public class PageFileTypeConfiguration : IEntityTypeConfiguration<PageFile>
    {
        public void Configure(EntityTypeBuilder<PageFile> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasIndex(p => p.PageIndex);
            builder.Property(p => p.PageIndex).HasMaxLength(EntityDefaults.MaxIndexLength);
            builder.Property(p => p.FileName).HasMaxLength(EntityDefaults.MaxStringLength).IsRequired();
            builder.Property(p => p.FilePath).HasMaxLength(EntityDefaults.MaxPathLength).IsRequired();
        }
    }
}