using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public class SourceFileTypeConfiguration : IEntityTypeConfiguration<SourceFile>
    {
        public void Configure(EntityTypeBuilder<SourceFile> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.FileName).HasMaxLength(EntityDefaults.MaxStringLength).IsRequired();
            builder.Property(s => s.FilePath).HasMaxLength(EntityDefaults.MaxPathLength).IsRequired();
            builder.Property(s => s.PageCount).HasDefaultValue(EntityDefaults.DefaultPageCount).IsRequired();
            builder.HasMany(s => s.Pages)
                .WithOne(p => p.SourceFile)
                .HasForeignKey(p => p.SourceFileId);
            builder.HasOne(s => s.FileConversion)
                .WithOne(f => f.SourceFile);
        }
    }
}