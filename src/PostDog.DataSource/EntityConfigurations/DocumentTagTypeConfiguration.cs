using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public class DocumentTagTypeConfiguration : IEntityTypeConfiguration<DocumentTag>
    {
        public void Configure(EntityTypeBuilder<DocumentTag> builder)
        {
            builder.HasKey(dt => new {dt.DocumentId, dt.TagId});

            builder.HasOne(dt => dt.Document)
                .WithMany(d => d.DocumentTags)
                .HasForeignKey(d => d.DocumentId);

            builder.HasOne(dt => dt.Tag)
                .WithMany(d => d.DocumentTags)
                .HasForeignKey(d => d.TagId);
        }
    }
}