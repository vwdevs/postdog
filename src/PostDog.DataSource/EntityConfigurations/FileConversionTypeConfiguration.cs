using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PostDog.Common.Globals;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public class FileConversionTypeConfiguration : IEntityTypeConfiguration<FileConversion>
    {
        public void Configure(EntityTypeBuilder<FileConversion> builder)
        {
            var enumConverter = new EnumToNumberConverter<ConversionState, int>();
            builder.HasKey(fc => fc.Id);
            builder.HasIndex(fc => fc.SourceFileId);
            builder.Property(fc => fc.State).HasConversion(enumConverter).IsRequired();
            builder.Property(fc => fc.Updated)
                .ValueGeneratedOnAdd()
                .HasDefaultValue(DateTime.MinValue)
                .IsRequired();
        }
    }
}