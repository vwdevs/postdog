using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public static class EntityTypeConfigurationExtension
    {
        /// <summary>
        ///     Applies all EntityTypeConfiguration for the PostDog entitites
        /// </summary>
        /// <param name="modelBuilder">
        ///     The builder being used to construct the model for this context.
        ///     Databases (and other extensions) typically define extension methods on this object
        ///     that allow you to configure aspects of the model that are specific to a given database
        /// </param>
        /// <returns>Instance of used model builder</returns>
        public static ModelBuilder ApplyEntityTypeConfiguration(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DocumentTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentTagTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FileConversionTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PageFileTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SourceFileTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TagTypeConfiguration());
            return modelBuilder;
        }

        /// <summary>
        ///     Customizes the default AspNetIdentity tables
        /// </summary>
        /// <param name="modelBuilder">
        ///     The builder being used to construct the model for this context.
        ///     Databases (and other extensions) typically define extension methods on this object
        ///     that allow you to configure aspects of the model that are specific to a given database
        /// </param>
        /// <returns>Instance of used model builder</returns>
        public static ModelBuilder ApplyIdentityModelCreating(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>().ToTable("Users");
            modelBuilder.Entity<IdentityUserRole<int>>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin<int>>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim<int>>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityRole<int>>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserToken<int>>().ToTable("UserTokens");
            modelBuilder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaims");
            return modelBuilder;
        }
    }
}