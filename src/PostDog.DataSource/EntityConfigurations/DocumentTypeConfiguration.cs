using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource.EntityConfigurations
{
    public class DocumentTypeConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.HasKey(d => d.Id);
            builder.Property(d => d.Sender).HasMaxLength(EntityDefaults.MaxCharLength);
            builder.Property(d => d.Description).HasMaxLength(EntityDefaults.MaxStringLength);
            builder.Property(d => d.CreatedDate)
                .ValueGeneratedOnAdd()
                .HasDefaultValue(DateTime.MinValue)
                .IsRequired();
            builder.Property(d => d.VirtualPath).HasDefaultValue(EntityDefaults.DefaultVirtualPath).IsRequired();

            var guidConverter = new GuidToStringConverter();
            builder.Property(d => d.DocumentGuid)
                .HasConversion(guidConverter)
                .ValueGeneratedOnAdd()
                .IsRequired();
            builder.Property(d => d.Slug).HasMaxLength(EntityDefaults.MaxCharLength);

            builder.HasMany(d => d.SourceFiles)
                .WithOne(s => s.Document)
                .HasForeignKey(s => s.DocumentId);
        }
    }
}