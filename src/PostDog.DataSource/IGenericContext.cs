using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using PostDog.DomainModels.Entities;

namespace PostDog.DataSource
{
    public interface IGenericContext: IDisposable
    {
        DatabaseFacade Database { get; }
        DbSet<Document> Documents { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task RunMigrationsAsync(CancellationToken cancellationToken);
    }
}