using System;
using PostDog.Common.Globals;

namespace PostDog.DataSource.Contexts
{
    public interface IContextFactory
    {
        public IGenericContext CreateContext();
        public IGenericContext CreateContext(PostDogSettings settings);
    }
}