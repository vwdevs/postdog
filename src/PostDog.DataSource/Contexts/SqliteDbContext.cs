﻿using Microsoft.EntityFrameworkCore;

namespace PostDog.DataSource.Contexts
{
    public class SqliteDbContext : GenericContext
    {
        public SqliteDbContext(DbContextOptions<SqliteDbContext> options) : base(options)
        {
        }
    }
}