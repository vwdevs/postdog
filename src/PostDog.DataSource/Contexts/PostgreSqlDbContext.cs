﻿using Microsoft.EntityFrameworkCore;

namespace PostDog.DataSource.Contexts
{
    public class PostgreSqlDbContext : GenericContext
    {
        public PostgreSqlDbContext(DbContextOptions<PostgreSqlDbContext> options) : base(options)
        {
        }
    }
}