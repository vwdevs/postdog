﻿using Microsoft.EntityFrameworkCore;

namespace PostDog.DataSource.Contexts
{
    public class SqlServerDbContext : GenericContext
    {
        public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options) : base(options)
        {
        }
    }
}