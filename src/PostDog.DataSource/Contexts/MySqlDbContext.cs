﻿using Microsoft.EntityFrameworkCore;

namespace PostDog.DataSource.Contexts
{
    public class MySqlDbContext : GenericContext
    {
        public MySqlDbContext(DbContextOptions<MySqlDbContext> options) : base(options)
        {
        }
    }
}