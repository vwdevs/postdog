using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;

namespace PostDog.DataSource.Contexts
{
    public class ContextFactory: IContextFactory
    {
        private readonly IOptionsSnapshot<PostDogSettings> _options;

        public ContextFactory(IOptionsSnapshot<PostDogSettings> options)
        {
            _options = options;
        }

        public IGenericContext CreateContext()
        {
            var type = _options.Value.Database.Type;
            var connectionString = _options.Value.Database.ConnectionString;

            return CreateContext(type, connectionString);
        }

        public IGenericContext CreateContext(PostDogSettings settings)
        {
            return CreateContext(settings.Database.Type, settings.Database.ConnectionString);
        }

        private IGenericContext CreateContext(DatabaseType type, string connectionString)
        {
            switch (type)
            {
                case DatabaseType.Sqlite:
                    var sqliteOptions = new DbContextOptionsBuilder<SqliteDbContext>()
                        .UseSqlite(connectionString)
                        .Options;
                    return new SqliteDbContext(sqliteOptions);
                
                case DatabaseType.MySql:
                    var mysqlOptions = new DbContextOptionsBuilder<MySqlDbContext>()
                        .UseMySql(connectionString)
                        .Options;
                    return new MySqlDbContext(mysqlOptions);
                
                case DatabaseType.PostgreSql:
                    var psqlOptions = new DbContextOptionsBuilder<PostgreSqlDbContext>()
                        .UseNpgsql(connectionString)
                        .Options;
                    return new PostgreSqlDbContext(psqlOptions);
                
                case DatabaseType.SqlServer:
                    var sqlOptions = new DbContextOptionsBuilder<SqlServerDbContext>()
                        .UseSqlServer(connectionString)
                        .Options;
                    return new SqlServerDbContext(sqlOptions);
                
                default:
                    throw new InvalidOperationException($"Unsupported database provider: {type}");
            }
        }
    }
}