using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.DataSource.Extensions
{
    public static class SqliteDbContextExtension
    {
        public static IServiceCollection AddSqliteDb(this IServiceCollection services)
        {
            services.AddDbContext<SqliteDbContext>((provider, options) =>
            {
                var dbConfig = provider.GetRequiredService<IOptions<PostDogSettings>>();
                options.UseSqlite(dbConfig.Value.Database.ConnectionString);
            });


            return services;
        }
    }
}