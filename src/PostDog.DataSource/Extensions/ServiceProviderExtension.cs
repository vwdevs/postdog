using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.DataSource.Extensions
{
    public static class ServiceProviderExtension
    {
        public static IGenericContext GetGenericDbContext(this IServiceProvider provider)
        {
            var dbConfig = provider.GetRequiredService<IOptionsSnapshot<PostDogSettings>>();
            return dbConfig.Value.Database.Type switch
            {
                DatabaseType.Sqlite => provider.GetRequiredService<SqliteDbContext>(),
                DatabaseType.SqlServer => provider.GetRequiredService<SqlServerDbContext>(),
                DatabaseType.MySql => provider.GetRequiredService<MySqlDbContext>(),
                DatabaseType.PostgreSql => provider.GetRequiredService<PostgreSqlDbContext>(),
                _ => throw new InvalidOperationException(
                    $"Unsupported database provider: {dbConfig.Value.Database.Type}")
            };
        }
    }
}