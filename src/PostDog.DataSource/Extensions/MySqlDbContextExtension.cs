using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.DataSource.Extensions
{
    public static class MySqlDbContextExtension
    {
        public static IServiceCollection AddMySqlDb(this IServiceCollection services)
        {
            services.AddDbContext<MySqlDbContext>((provider, options) =>
            {
                var dbConfig = provider.GetRequiredService<IOptionsSnapshot<PostDogSettings>>();
                options.UseMySql(dbConfig.Value.Database.ConnectionString);
            });
            return services;
        }
    }
}
