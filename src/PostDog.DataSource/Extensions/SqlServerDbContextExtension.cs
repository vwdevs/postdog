using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.DataSource.Extensions
{
    public static class SqlServerDbContextExtension
    {
        public static IServiceCollection AddSqlServerDb(this IServiceCollection services)
        {
            services.AddDbContext<SqlServerDbContext>((provider, options) =>
            {
                var dbConfig = provider.GetRequiredService<IOptionsSnapshot<PostDogSettings>>();
                options.UseSqlServer(dbConfig.Value.Database.ConnectionString);
            });
            return services;
        }
    }
}
