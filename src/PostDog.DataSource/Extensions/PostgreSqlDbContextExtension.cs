using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.DataSource.Extensions
{
    public static class PostgreSqlDbContextExtension
    {
        public static IServiceCollection AddPostgreSqlDb(this IServiceCollection services)
        {
            services.AddDbContext<PostgreSqlDbContext>((provider, options) =>
            {
                var dbConfig = provider.GetRequiredService<IOptionsSnapshot<PostDogSettings>>();
                options.UseNpgsql(dbConfig.Value.Database.ConnectionString);
            });
            return services;
        }
    }
}
