const path = require("path");
const ESLintPlugin = require("eslint-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const AutoPrefixer = require("autoprefixer");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        main: [
            path.resolve(__dirname, "Assets", "Scripts", "src", "js", "app.js"),
            path.resolve(__dirname, "Assets", "css", "main.css"),
        ],
        vendors: [
            path.resolve(
                __dirname,
                "Assets",
                "Scripts",
                "src",
                "js",
                "libs.js"
            ),
        ],
    },

    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env"],
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "dist",
                        },
                    },
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [AutoPrefixer],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.(eot|woff|woff2|ttf)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: "fonts",
                            publicPath: "fonts",
                        },
                    },
                ],
            },
            {
                test: /\.(svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: "svg",
                            publicPath: "svg",
                        },
                    },
                ],
            },
        ],
    },

    output: {
        path: path.resolve(__dirname, "wwwroot", "dist"),
        filename: "[name].[contenthash].js",
    },

    optimization: {
        runtimeChunk: "single",
        splitChunks: {
            chunks: "all",
            maxInitialRequests: Infinity,
            minSize: 0,
        },
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin({
                test: /\.css(\?.*)?$/i,
            }),
        ],
    },

    plugins: [
        new ESLintPlugin({
            failOnWarning: false,
            failOnError: true,
            files: path.resolve(
                __dirname,
                "Assets",
                "Scripts",
                "src",
                "js",
                "*.js"
            ),
        }),
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: true,
            cleanOnceBeforeBuildPatterns: true,
            cleanStaleWebpackAssets: true,
            dangerouslyAllowCleanPatternsOutsideProject: true,
            dry: false,
        }),
        new HtmlWebpackPlugin({
            inject: "body",
            filename: path.resolve(
                __dirname,
                "Views",
                "Shared",
                "_Layout.cshtml"
            ),
            template: path.resolve(
                __dirname,
                "Views",
                "Shared",
                "_BaseLayout.cshtml"
            ),
            publicPath: "dist",
            minify: false,
        }),
        new HtmlWebpackPlugin({
            inject: "body",
            filename: path.resolve(
                __dirname,
                "Views",
                "Shared",
                "_SetupLayout.cshtml"
            ),
            template: path.resolve(
                __dirname,
                "Views",
                "Shared",
                "_BaseSetupLayout.cshtml"
            ),
            publicPath: "dist",
            minify: false,
        }),
        new MiniCssExtractPlugin({
            filename: "[name].[contenthash].css",
        }),
        new CopyPlugin({
            patterns: [
                {
                    from:
                        "node_modules/office-ui-fabric-core/dist/css/fabric.min.css",
                },
            ],
        }),
    ],
};
