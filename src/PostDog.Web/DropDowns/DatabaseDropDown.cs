using Microsoft.AspNetCore.Mvc.Rendering;
using PostDog.Common.Globals;

namespace PostDog.Web.DropDowns
{
    public class DatabaseDropDown
    {
        public static SelectListItem Sqlite = new SelectListItem(nameof(DatabaseType.Sqlite), DatabaseType.Sqlite.ToString());
        public static SelectListItem MySql = new SelectListItem(nameof(DatabaseType.MySql), DatabaseType.MySql.ToString());
        public static SelectListItem PostgreSql = new SelectListItem(nameof(DatabaseType.PostgreSql), DatabaseType.PostgreSql.ToString());
        public static SelectListItem MsSql = new SelectListItem(nameof(DatabaseType.SqlServer), DatabaseType.SqlServer.ToString());
    }
}