using Microsoft.AspNetCore.Mvc.Rendering;
using PostDog.Common.Labels;

namespace PostDog.Web.DropDowns
{
    public class CommonDropDown
    {
        public static SelectListItem PleaseSelect = new SelectListItem(ApplicationLabels.DROP_DOWN_Please_Select, string.Empty, true);
    }
}