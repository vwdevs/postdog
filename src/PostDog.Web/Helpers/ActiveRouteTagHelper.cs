using Microsoft.AspNetCore.Razor.TagHelpers;

namespace PostDog.Web.Helpers
{
    [HtmlTargetElement(Attributes = "isactive")]
    public class ActiveRouteTagHelper: TagHelper
    {
        [HtmlAttributeName("asp-controller")]
        public string Controller { get; set; }
        
        [HtmlAttributeName("asp-action")]
        public string Action { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
        }
    }
}