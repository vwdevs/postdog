using Microsoft.AspNetCore.Mvc.Rendering;

namespace PostDog.Web.Helpers
{
    public static class CssHtmlHelper
    {
        public static string IsInvisible(this IHtmlHelper html, bool isVisible)
        {
            return isVisible ? string.Empty : "is-invisible";
        }
    }
}
