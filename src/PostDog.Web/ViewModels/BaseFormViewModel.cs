using PostDog.Protocol.Validation;

namespace PostDog.Web.ViewModels
{
    public abstract class BaseFormViewModel
    {
        public BaseFormViewModel()
        {
            Rule = new FormValidator();
        }
        internal FormValidator Rule { get; set; }
    }
}