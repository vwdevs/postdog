using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using PostDog.Common.Globals;
using PostDog.Protocol.Models.Setup;
using PostDog.Web.DropDowns;

namespace PostDog.Web.ViewModels
{
    public class SetupViewModel: BaseFormViewModel, IValidatableObject
    {
        public SetupViewModel()
        {
            DataBaseSelectList = new List<SelectListItem>
            {
                CommonDropDown.PleaseSelect,
                DatabaseDropDown.Sqlite,
                DatabaseDropDown.MySql,
                DatabaseDropDown.PostgreSql,
                DatabaseDropDown.MsSql
            };
        }
        public IEnumerable<SelectListItem> DataBaseSelectList { get; set; }
        public string DatabaseProvider { get; set; }

        public bool ShowDatabaseFields => !string.IsNullOrWhiteSpace(DatabaseProvider) &&
                                          !DatabaseProvider.Equals(DatabaseDropDown.Sqlite.Value);

        public string DatabaseHost { get; set; }
        public string DatabasePort { get; set; }
        public string DatabaseUser { get; set; }
        public string DatabasePassword { get; set; }
        public string DatabaseName { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordRepeat { get; set; }

        public SetupRequest GetSetupData()
        {
            return new SetupRequest
            {
                DatabaseProvider = DatabaseProvider,
                DatabaseHost = DatabaseHost,
                DatabasePort = DatabasePort,
                DatabaseUser = DatabaseUser,
                DatabaseUserPassword = DatabasePassword,
                DatabaseName = DatabaseName,
                InitialUsername = Username,
                UserPassword = Password
            };
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            Rule.For(nameof(DatabaseProvider)).NotEmpty().Verify(DatabaseProvider);

            if (!DatabaseProvider.Equals(nameof(DatabaseType.Sqlite)))
            {
                Rule.For(nameof(DatabaseHost)).NotEmpty().Verify(DatabaseHost);
                Rule.For(nameof(DatabasePort)).NotEmpty().Verify(DatabasePort);
                Rule.For(nameof(DatabaseName)).NotEmpty().Verify(DatabaseName);
                Rule.For(nameof(DatabaseUser)).NotEmpty().Verify(DatabaseUser);
                Rule.For(nameof(DatabasePassword)).NotEmpty().Verify(DatabasePassword);
            }

            Rule.For(nameof(Username)).NotEmpty().Verify(Username);
            Rule.For(nameof(Password)).NotEmpty().Verify(Password);
            Rule.For(nameof(PasswordRepeat)).NotEmpty().SameAs(Password).Verify(PasswordRepeat);

            foreach (var validationResult in Rule.ValidationResults)
            {
                yield return validationResult;
            }
        }
    }
}
