using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PostDog.DomainModels.Entities;
using PostDog.Protocol.Models.Auth;

namespace PostDog.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthController(SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpPost(nameof(Login))]
        public async Task<IActionResult> Login([FromBody] AuthenticateRequest request)
        {
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null) return Unauthorized();

            var result = await _signInManager
                .CheckPasswordSignInAsync(user, request.Password, false);
            if (!result.Succeeded) return Unauthorized();

            return Ok();
        }

        [HttpPost(nameof(Register))]
        public async Task<IActionResult> Register([FromBody] RegisterRequest request)
        {
            // var user = _authFactory.BuildApplicationUser(request);
            //
            // var result = await _userManager.CreateAsync(user, request.Password);
            // if (!result.Succeeded) return BadRequest(result.Errors);
            //
            // return Ok(_authFactory.BuildRegisterResponse());
            return Ok();
        }
    }
}