using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PostDog.Services.Services;
using PostDog.Web.ViewModels;

namespace PostDog.Web.Controllers
{
    public class SetupController : Controller
    {
        private readonly SetupService _setupService;

        public SetupController(SetupService setupService)
        {
            _setupService = setupService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var viewModel = new SetupViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Index(SetupViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                await _setupService.SetupPostdog(viewModel.GetSetupData());
                return RedirectToAction("Index", "Home");
            }

            return View(viewModel);
        }
    }
}
