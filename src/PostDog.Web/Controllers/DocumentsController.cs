﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PostDog.Protocol.Models.Documents;
using PostDog.Services.Services;

namespace PostDog.Web.Controllers
{
    public class DocumentsController : Controller
    {
        private readonly DocumentService _documentService;
        private readonly ILogger<DocumentsController> _logger;

        public DocumentsController(DocumentService documentService, ILogger<DocumentsController> logger)
        {
            _documentService = documentService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index([FromQuery] DocumentRequest request)
        {
             var result = _documentService.GetPaginatedDocuments(request.SkipPages, request.PageOffset);
            return View();
        }

        [HttpGet]
        public IActionResult ShowUploadModal()
        {
            return PartialView("_uploadModal");
        }
    }
}
