import SetupView from "../views/setup.view";
import DocumentsView from "../views/documents.view";

export const createViews = () => {
    SetupView.init();
    DocumentsView.init();
};
