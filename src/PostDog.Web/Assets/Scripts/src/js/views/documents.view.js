import routes from "../core/routes";

function bindEvents() {
    const uploadButton = document.getElementById("document-upload-id");
    if (uploadButton !== null) {
        uploadButton.addEventListener("click", raiseModal);
    }
}

function raiseModal() {
    const modal = document.getElementById("placeholder");
    if (modal !== null) {
        fetch(routes.Documents.UploadModalApi)
            .then((response) => response.text())
            .then((data) => {
                modal.innerHTML = data;
            });
    }
}

export default {
    init: bindEvents,
};
