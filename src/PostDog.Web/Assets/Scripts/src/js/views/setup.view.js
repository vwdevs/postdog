function bindEvents() {
    const dropDown = document.getElementById("database-select");
    if (dropDown !== null) {
        dropDown.addEventListener("change", handleChange);
    }

    const submitButton = document.getElementById("setup-submit");
    if (submitButton !== null) {
        submitButton.addEventListener("click", handleSubmit);
    }
}

function handleChange() {
    const databaseType = document.getElementById("database-select");
    const databaseHost = document.getElementById("database-host");
    const databasePort = document.getElementById("database-port");
    const databaseUser = document.getElementById("database-user");
    const databasePassword = document.getElementById("database-password");
    const databaseName = document.getElementById("database-name");

    const selectedType = databaseType.value;

    function toggleRowVisibility(row) {
        const formRow = row;
        const validation = row.nextElementSibling;
        if (selectedType === "Sqlite" || selectedType === "") {
            formRow.classList.add("is-invisible");
            validation.classList.add("is-invisible");
        } else {
            formRow.classList.remove("is-invisible");
            validation.classList.remove("is-invisible");
        }
    }

    if (typeof databaseHost !== "undefined") {
        const formRow = databaseHost.closest(".form-row");
        toggleRowVisibility(formRow);
    }

    if (typeof databasePort !== "undefined") {
        const formRow = databasePort.closest(".form-row");
        toggleRowVisibility(formRow);
    }

    if (typeof databaseName !== "undefined") {
        const formRow = databaseName.closest(".form-row");
        toggleRowVisibility(formRow);
    }

    if (typeof databaseUser !== "undefined") {
        const formRow = databaseUser.closest(".form-row");
        toggleRowVisibility(formRow);
    }

    if (typeof databasePassword !== "undefined") {
        const formRow = databasePassword.closest(".form-row");
        toggleRowVisibility(formRow);
    }
}

function handleSubmit(event) {
    event.target.classList.add("is-loading");
}

export default {
    init: bindEvents,
};
