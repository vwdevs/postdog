using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;

namespace PostDog.Cli.Extensions
{
    public static class ServiceCollectionExtensions
    {

        public static OptionsBuilder<PostDogSettings> ConfigurePostDogSettings(
            this OptionsBuilder<PostDogSettings> optionsBuilder,
            string[] args)
        {
            return optionsBuilder.Configure(ConfigureOptions(args));
        }

        private static Action<PostDogSettings> ConfigureOptions(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            return configuration.Bind;
        }
    }
}