﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using PostDog.Protocol.Models.Setup;
using PostDog.Services.Services;

namespace PostDog.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ServiceBuilder.CreateServiceProvider(args);

            if (args.Length == 0)
            {
                Console.WriteLine("No arguments passed. Use the flag -h to get a list of available commmands");
            }

            if (args.Contains("-h"))
            {
                Console.WriteLine("Help");
            }

            if (args.Contains("-m") || args.Contains("migrate"))
            {
                Console.WriteLine("Applying migrations...");
                var setupService = serviceProvider.GetService<SetupService>();
                setupService.SetupPostdog(new SetupRequest());
                Console.WriteLine("Done");
            }

            serviceProvider.Dispose();
        }
    }
}
