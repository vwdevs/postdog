FROM mcr.microsoft.com/dotnet/runtime:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY /src .
RUN dotnet restore PostDog.Cli
RUN dotnet build PostDog.Cli -c Release -o /app

FROM build AS publish
RUN dotnet publish PostDog.Cli -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "PostDog.Cli.dll"]