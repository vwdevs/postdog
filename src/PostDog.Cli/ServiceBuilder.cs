using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Cli.Extensions;
using PostDog.Common.Globals;
using PostDog.DataSource;
using PostDog.DataSource.Contexts;
using PostDog.DataSource.Extensions;
using PostDog.DomainModels.Entities;
using PostDog.Services.Services;

namespace PostDog.Cli
{
    public class ServiceBuilder
    {
        public static ServiceProvider CreateServiceProvider(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddOptions<PostDogSettings>()
                             .ConfigurePostDogSettings(args);
            serviceCollection.AddScoped(GetGenericDbContext);
            serviceCollection.AddSqliteDb()
                             .AddSqlServerDb()
                             .AddPostgreSqlDb()
                             .AddMySqlDb();

            serviceCollection.AddIdentityCore<ApplicationUser>()
                             .AddEntityFrameworkStores<SqliteDbContext>()
                             .AddEntityFrameworkStores<MySqlDbContext>()
                             .AddEntityFrameworkStores<PostgreSqlDbContext>()
                             .AddEntityFrameworkStores<SqlServerDbContext>();

            serviceCollection.AddTransient<SetupService>();

            var serviceProvider = serviceCollection.BuildServiceProvider();
            return serviceProvider;
        }


        private static IGenericContext GetGenericDbContext(IServiceProvider provider)
        {
            var dbConfig = provider.GetRequiredService<IOptions<PostDogSettings>>();
            return dbConfig.Value.Database.Type switch
            {
                DatabaseType.Sqlite => provider.GetRequiredService<SqliteDbContext>(),
                DatabaseType.SqlServer => provider.GetRequiredService<SqlServerDbContext>(),
                DatabaseType.MySql => provider.GetRequiredService<MySqlDbContext>(),
                DatabaseType.PostgreSql => provider.GetRequiredService<PostgreSqlDbContext>(),
                _ => throw new InvalidOperationException(
                    $"Unsupported database provider: {dbConfig.Value.Database.Type}")
            };
        }
    }
}