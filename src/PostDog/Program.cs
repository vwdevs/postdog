using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using PostDog.Common.Globals;
using PostDog.Extensions;

namespace PostDog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host
                .CreateDefaultBuilder(args)
                .InitializeAppData()
                .ConfigureAppConfiguration((ctx, config) =>
                {
                    var configRoot = Environment.GetEnvironmentVariable("POSTDOG_CONFIG_ROOT");
                    if (!string.IsNullOrEmpty(configRoot)) config.SetBasePath(configRoot);
                    config.AddJsonFile(AppData.SettingsFile, true, true);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(options => options.Limits.MaxRequestBodySize = null);
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}