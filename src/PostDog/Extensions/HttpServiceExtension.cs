using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PostDog.Configuration;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace PostDog.Extensions
{
    public static class HttpServiceExtension
    {
        public static IServiceCollection ConfigureHttpServices(this IServiceCollection services, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                services.AddControllersWithViews()
                        .AddRazorRuntimeCompilation();
                
                services.Configure<MvcRazorRuntimeCompilationOptions>(options =>
                {
                    var libraryPath = Path.GetFullPath(Path.Combine(env.ContentRootPath, "..", "PostDog.Web"));
                    options.FileProviders.Add(new PhysicalFileProvider(libraryPath));
                });
            }
            else services.AddControllersWithViews();

            services.AddCors();
            services.AddHttpContextAccessor();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            services.AddSingleton<IConfigureOptions<ForwardedHeadersOptions>, ForwardingHeaderConfiguration>();
            services.AddRouting(config =>
            {
                config.LowercaseUrls = true;
                config.LowercaseQueryStrings = true;
            });
            
            return services;
        }
    }
}