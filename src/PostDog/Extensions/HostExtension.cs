using System;
using Microsoft.Extensions.Hosting;
using PostDog.Common.Globals;
using PostDog.Services.Services;

namespace PostDog.Extensions
{
    public static class HostExtension
    {
        public static IHostBuilder InitializeAppData(this IHostBuilder host)
        {
            var service = new ConfigService();
            service.InitializeAppData(CreateDefaultSettings());
            return host;
        }

        private static PostDogSettings CreateDefaultSettings()
        {
            var defaultSettings = new PostDogSettings
            {
                IsSetup = false,
                Token = Guid.NewGuid().ToString("n"),
                Database = new DatabaseSettings
                {
                    Type = DatabaseType.Sqlite,
                    ConnectionString = "Data Source=appdata/postdog.db"
                }
            };
            return defaultSettings;
        }
    }
}