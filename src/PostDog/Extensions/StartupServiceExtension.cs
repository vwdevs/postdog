using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;
using PostDog.DataSource.Extensions;
using PostDog.DomainModels.Entities;
using PostDog.Services.Services;

namespace PostDog.Extensions
{
    public static class StartupServiceExtension
    {
        /// <summary>
        ///     This method bootstraps all PostDog services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddPostDogServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            services.Configure<PostDogSettings>(configuration);
            services.TryAddScoped(provider => provider.GetGenericDbContext());
            services.TryAddTransient<DocumentService>();
            services.TryAddTransient<SetupService>();
            services.TryAddTransient<ConfigService>();
            services.TryAddTransient<MigrationService>();
            services.TryAddTransient<IContextFactory, ContextFactory>();

            return services;
        }

        public static IServiceCollection AddIdentityServices(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddIdentityCore<ApplicationUser>()
                    .AddUserManager<UserManager<ApplicationUser>>()
                    .AddSignInManager()
                    .AddEntityFrameworkStores<SqliteDbContext>()
                    .AddEntityFrameworkStores<MySqlDbContext>()
                    .AddEntityFrameworkStores<PostgreSqlDbContext>()
                    .AddEntityFrameworkStores<SqlServerDbContext>();

            services.AddAuthentication();

            return services;
        }
    }
}