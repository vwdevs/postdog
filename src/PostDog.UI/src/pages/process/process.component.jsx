import React from 'react';
import './process.styles.scss';
import ProcessesTableComponent from "../../components/processes-collection/processes-table.component";

const INITIAL_DATA = [
  {
    id: '#10528',
    filename: 'Amazon_Invoice_Laptop.pdf',
    updated: '2020-02-01',
    progress: 45
  },
  {
    id: '#10430',
    filename: 'Gucci_Invoice_Belt.pdf',
    updated: '3 days ago',
    progress: 100
  },
  {
    id: '#10131',
    filename: 'Allianz_Contract.pdf',
    updated: '24 minutes ago',
    progress: 66
  },
  {
    id: '#10129',
    filename: 'Letter.pdf',
    updated: '12 minutes ago',
    progress: 12
  },
];

const ProcessPage = () => {
    return (
        <div className="process-page">
            <nav className='content-tabs'>
                <ul>
                    <li className='active-tab'><a>Processed Documents</a></li>
                    <li><a>Processed Emails</a></li>
                    <li><a>Failed Processes</a></li>
                </ul>
            </nav>
            <div className="content-panel content-panel--left-top-radius-deactivate">
                <ProcessesTableComponent processedDocs={INITIAL_DATA}/>
            </div>
        </div>
    );
};

export default ProcessPage;
