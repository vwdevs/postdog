import React from 'react';
import '../../shared/layout/base.scss';
import GridContainerComponent from '../../shared/layout/grid-container/grid-container.component';
import SetupPage from '../setup/setup.component';
import { Route } from 'react-router-dom';

function App() {
    return (
        <div className="app">
            <Route exact path="/setup" component={SetupPage} />
            <Route path="/" component={GridContainerComponent} />
        </div>
    );
}

export default App;
