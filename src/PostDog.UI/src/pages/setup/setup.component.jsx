import React, {useState} from 'react';
import './setup.styles.scss';
import {FontIcon} from "office-ui-fabric-react/lib/Icon";



const SetupPage = () => {
    const [currentStep, setStep] = useState(1);
    
    return (
        <div className="setup-page">
            <div className='setup__panel'>
                <div className='setup__panel-header'>
                    <h1>SETUP</h1>
                </div>

                <div className='setup__panel-progress-nav'>
                    <div>
                        <FontIcon
                            iconName="RadioBtnOn"
                            className="fabric-ui-icon fabric-ui-icon--progress"
                        />
                        Database
                    </div>
                    <div>
                        <FontIcon
                            iconName="RadioBtnOff"
                            className="fabric-ui-icon fabric-ui-icon--progress"
                        />
                        User Setup
                    </div>
                </div>

                <div className='setup__panel-form'>
                    <div className='database-types'>
                        <div>
                            <input type="radio" name="SQlite" id=""/> SQlite
                        </div>

                        <div>
                            <input type="radio" name="PostgreSQL" id="" /> PostgreSQL
                        </div>

                        <div>
                            <input type="radio" name="MariaDB" id="" /> MariaDB
                        </div>

                        <div>
                            <input type="radio" name="MSSQL" id="" /> MSSQL
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SetupPage;
