import React from 'react';

const UserPage = () => {
    return (
        <div className='user-page'>
            <div className="user-page__backdrop" />
            <div className='user-page__body'>
                <div className='user-page__close'/>
                User Page
            </div>
        </div>
    );
};

export default UserPage;