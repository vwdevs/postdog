import React, { useState } from 'react';
import '../../shared/layout/base.scss';
import './documents.styles.scss';
import DocumentDetailComponent from '../../components/document-detail/document-detail.component';
import NewDocumentComponent from '../../components/new-document/new-document.component';
import {
    AddDocumentActionComponent, FolderActionComponent,
    HelpActionComponent,
    ProcessActionComponent,
} from '../../shared/components/actions.component';
import FullSearchComponent from '../../shared/components/full-search/full-search.component';
import DocumentsTableComponent from '../../components/documents-collection/documents-table.component';
import { TagPopupComponent } from '../../shared/components/popups/popup.component';
import { ToastsContainerComponent } from '../../shared/components/toasts/toasts.component';
import ToastyTypes from '../../shared/components/toasts/toasty.types';

const INITIAL_DATA = [
    {
        id: 1,
        from: 'Amazon',
        tags: ['Invoice', 'Purchase Order', 'Confirmation'],
        description: 'Invoice for laptop, 1300 €',
        date: '2020-02-01',
        pages: '3',
    },
    {
        id: 2,
        from: 'Gucci',
        tags: ['Invoice'],
        description: 'Invoice for belt',
        date: '2020-02-01',
        pages: '1',
    },
    {
        id: 3,
        from: 'Allianz',
        tags: ['Contract'],
        description: 'Insurance contract',
        date: '2020-02-01',
        pages: '13',
    },
    {
        id: 4,
        from: 'Grandma',
        tags: ['Personal letter'],
        description: 'I made your favourite cake',
        date: '2020-02-01',
        pages: '3',
    },
];

const INITIAL_TAGS = ['Invoice', 'Contract', 'Purchase Order', 'Letter'];

const DocumentsPages = () => {
    let [showModal, setVisibility] = useState(false);
    let [showTagPopup, setPopupVisibility] = useState(false);
    let [showNewDocument, setDocumentVisibility] = useState(false);
    let [documents, setDocuments] = useState(INITIAL_DATA);
    let [tags, setTags] = useState(INITIAL_TAGS);
    let [toasties, setToasties] = useState([]);

    const filterDocuments = event => {
        const searchValue = event.target.value.toLowerCase();
        if (event.target.value !== '') {
            setDocuments(
                INITIAL_DATA.filter(
                    doc =>
                        doc.from.toLowerCase().includes(searchValue) ||
                        doc.description.toLowerCase().includes(searchValue) ||
                        doc.tags.some(tag =>
                            tag.toLowerCase().includes(searchValue)
                        )
                )
            );
        } else {
            setDocuments(INITIAL_DATA);
        }
    };

    const filterDocumentsByTags = activeTags => {
        if (activeTags.length > 0) {
            setDocuments(
                INITIAL_DATA.filter(doc =>
                    doc.tags.some(tag => activeTags.includes(tag.toLowerCase()))
                )
            );
        } else {
            setDocuments(INITIAL_DATA);
        }
    };

    const deleteDocumentById = id => {
        let deletedDocument = documents.find(doc => doc.id === id);
        let documentToasty = {
            type: ToastyTypes.SUCCESS,
            file: deletedDocument.description,
        };
        setDocuments(documents.filter(doc => doc.id !== id));
        setToasties([documentToasty]);
    };

    function filterToastiesById(index) {
        setToasties(
            toasties.filter(toasty => toasties.indexOf(toasty) !== index)
        );
    }

    return (
        <div className="documents-page">
            <div className="content-header">
                <FullSearchComponent
                    placeHolder={'Search here...'}
                    onChange={filterDocuments}
                />

                <div className="content-header__action-list">
                    <FolderActionComponent />
                    <ProcessActionComponent />
                    <AddDocumentActionComponent
                        handleClick={() => setDocumentVisibility(true)}
                    />
                    <HelpActionComponent />
                </div>
            </div>

            <div className="content-panel">
                <DocumentsTableComponent
                    handleClick={() => setVisibility(true)}
                    handleTagClick={() => setPopupVisibility(!showTagPopup)}
                    handleDelete={deleteDocumentById}
                    documents={documents}
                >
                    {showTagPopup && (
                        <TagPopupComponent
                            tags={tags}
                            filterDocuments={filterDocumentsByTags}
                            handleClose={() => setPopupVisibility(false)}
                        />
                    )}
                </DocumentsTableComponent>
            </div>

            {showNewDocument && (
                <NewDocumentComponent
                    handleClose={() => setDocumentVisibility(false)}
                />
            )}
            {showModal && (
                <DocumentDetailComponent
                    handleClose={() => setVisibility(false)}
                />
            )}

            {toasties.length > 0 && (
                <ToastsContainerComponent
                    toasties={toasties}
                    filterToasties={filterToastiesById}
                />
            )}
        </div>
    );
};

export default DocumentsPages;
