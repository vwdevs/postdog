import React from 'react';
import ReactDOM from 'react-dom';
import './shared/layout/main.scss';
import App from './pages/App/App';

import { initializeIcons } from '@uifabric/icons';
import { BrowserRouter } from 'react-router-dom';
initializeIcons();

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('root')
);
