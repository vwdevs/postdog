import React, { useState } from 'react';
import './document-detail.styles.scss';
import logo1 from './img.png';
import logo2 from './img2.png';
import logo3 from './img3.png';
import logo4 from './img4.png';
import thumb1 from './thumb.png';
import thumb2 from './thumb2.png';
import thumb3 from './thumb3.png';
import thumb4 from './thumb4.png';
import {
    PageLabelComponent,
    PaginateButtonComponent,
} from './detail-action-buttons/paginate-button.component';
import {
    EditButtonComponent,
    FavButtonComponent,
    PrintButtonComponent,
} from './detail-action-buttons/action-buttons.component';

const INITIAL_THUMBNAILS = [thumb1, thumb2, thumb3, thumb4];

const getImageSource = key => {
    switch (key) {
        case 0:
            return logo1;
        case 1:
            return logo2;
        case 2:
            return logo3;
        case 3:
            return logo4;
        default:
            return logo1;
    }
};

const DocumentDetailComponent = ({ handleClose }) => {
    const [image, setImageSource] = useState(getImageSource());
    const [currentPage, setPage] = useState(1);

    function increment() {
        if (currentPage < INITIAL_THUMBNAILS.length) {
            setPage(page => page + 1);
            setImageSource(getImageSource(currentPage));
        }
    }

    function decrease() {
        if (currentPage > 1) {
            setPage(page => page - 1);
            setImageSource(getImageSource(currentPage));
        }
    }

    return (
        <div className="document-modal">
            <div className="document-modal__backdrop" onClick={handleClose} />
            <div className="document-modal__body">
                <div className="document__button-list">
                    <FavButtonComponent />
                    <PrintButtonComponent />
                    <EditButtonComponent />
                </div>

                <div className="document-detail">
                    <div
                        className="document-modal__close"
                        onClick={handleClose}
                    />

                    <div className="document__pagination">
                        <PaginateButtonComponent
                            icon="Previous"
                            handleClick={decrease}
                        />
                        <PageLabelComponent
                            currentPage={currentPage}
                            pageCount={INITIAL_THUMBNAILS.length}
                        />
                        <PaginateButtonComponent
                            icon="Next"
                            handleClick={increment}
                        />
                    </div>
                    <div className="document__page">
                        <img
                            src={image}
                            className="document-image"
                            alt="document page"
                        />
                    </div>
                    <div className="document__zoom">
                        <select name="zoom" id="zoom" defaultValue="100">
                            <option value="75">75 %</option>
                            <option value="100">100 %</option>
                            <option value="125">125 %</option>
                            <option value="150">150 %</option>
                        </select>
                    </div>
                </div>
                <div className="document-pages">
                    {INITIAL_THUMBNAILS.map((thumbnail, idx) => (
                        <img
                            key={idx}
                            src={thumbnail}
                            className="document-image document-image--thumbnail"
                            alt="document-image"
                            onClick={() => {
                                setImageSource(getImageSource(idx));
                                setPage(idx + 1);
                            }}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
};

export default DocumentDetailComponent;
