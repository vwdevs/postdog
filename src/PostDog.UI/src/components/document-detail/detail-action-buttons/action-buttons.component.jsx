import React from "react";
import { FontIcon } from "office-ui-fabric-react/lib/Icon";


export const FavButtonComponent = ({handleClick}) => {
  return (
    <div className="action" onClick={handleClick}>
      <FontIcon iconName="FavoriteStarFill" className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const EditButtonComponent = ({handleClick}) => {
  return (
    <div className="action" onClick={handleClick}>
      <FontIcon iconName="Edit" className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const PrintButtonComponent = ({handleClick}) => {
  return (
    <div className="action" onClick={handleClick}>
      <FontIcon iconName="CloudDownload" className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};