import React from "react";
import { FontIcon } from "office-ui-fabric-react/lib/Icon";
import './pagination.styles.scss';

export const PaginateButtonComponent = ({icon, handleClick}) => {
  return (
    <div className="action button--add" onClick={handleClick}>
      <FontIcon iconName={icon} className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const PageLabelComponent = ({currentPage, pageCount}) => {
  return (
    <div className='page-label'>
      {currentPage} / {pageCount}
    </div>
  );
};