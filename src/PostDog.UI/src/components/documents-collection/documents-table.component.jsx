import React, { useState } from 'react';
import './documents-table.styles.scss';
import { FontIcon } from 'office-ui-fabric-react/lib/Icon';

const DocumentsTableComponent = props => {
    let [deleteState, setDeleteState] = useState('Delete');

    const toggleDeleteState = docId => {
        if (deleteState === 'Delete') {
            setDeleteState('CheckMark');
            return;
        }
        setDeleteState('Delete');
        props.handleDelete(docId);
    };

    return (
        <table className="documents-table">
            <colgroup>
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th>From</th>
                    <th>
                        Tags
                        <FontIcon
                            iconName="MoreVertical"
                            className="fabric-ui-icon fabric-ui-icon--more"
                            onClick={props.handleTagClick}
                        />
                        {props.children}
                    </th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Pages</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {props.documents.map(doc => (
                    <tr key={doc.id} onDoubleClick={props.handleClick}>
                        <td>{doc.from}</td>
                        <td>
                            {doc.tags.map((tag, index) => (
                                <div key={index} className="tag">
                                    {tag}
                                </div>
                            ))}
                        </td>
                        <td>{doc.description}</td>
                        <td>{doc.date}</td>
                        <td>{doc.pages}</td>
                        <td>
                            <div className="action-list">
                                <div className="action action--mini">
                                    <FontIcon
                                        iconName="Edit"
                                        className="fabric-ui-icon"
                                    />
                                </div>

                                <div
                                    className={
                                        deleteState === "CheckMark"
                                            ? "action action--mini action--danger"
                                            : "action action--mini"
                                    }
                                    onClick={() => toggleDeleteState(doc.id)}
                                    onMouseLeave={() =>
                                        setDeleteState('Delete')
                                    }
                                >
                                    <FontIcon
                                        iconName={deleteState}
                                        className="fabric-ui-icon"
                                    />
                                </div>
                            </div>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default DocumentsTableComponent;
