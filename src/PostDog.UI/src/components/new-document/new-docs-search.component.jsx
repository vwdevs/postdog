import React, { useState } from 'react';
import {FontIcon} from "office-ui-fabric-react/lib/Icon";

const INITIAL_TAGS = ['Invoice', 'Contract', 'Purchase Order', 'Letter'];
const INITIAL_FROMS = [
    'Amazon',
    'Dell',
    'Samsung',
    'Canada Life',
    'Google',
    'Microsoft',
    'Huawei',
];

export const TagSearchComponent = () => {
    let [tags, setTags] = useState([]);

    const addEnterTags = event => {
      if (event.keyCode === 13) addTags(event);
    };
    
    const addTags = event => {
      if (event.target.value !== '') {
        setTags([...tags, event.target.value]);
        event.target.value = '';
      }
    };

    function removeTags(index) {
        setTags([...tags.filter(tag => tags.indexOf(tag) !== index)]);
    }

    return (
        <div className="search-input-group search-input-group--space">
            <ul className="search-tag">
                {tags.map((tag, index) => (
                    <li key={index}>
                        <span>{tag}</span>
                        <div
                            className="search-tag__close"
                            onClick={() => removeTags(index)}
                        />
                    </li>
                ))}
            </ul>

            <input
                type="text"
                placeholder="..."
                className="search-input search-input--docs"
                list="tag-data"
                onKeyDown={addEnterTags}
                onBlur={addTags}
            />
            <datalist id="tag-data">
                {INITIAL_TAGS.map((tag, index) => (
                    <option key={index} value={tag} />
                ))}
            </datalist>
        </div>
    );
};

export const FromSearchComponent = () => {
    return (
        <div className="search-input-group search-input-group--space">
            <input
                type="text"
                placeholder="..."
                className="search-input search-input--docs"
                list="data"
            />

            <datalist id="data">
                {INITIAL_FROMS.map((from,index) => (
                    <option key={index} value={from} />
                ))}
            </datalist>
        </div>
    );
};

export const DescriptionComponent = () => {
    return (
        <div className="search-input-group search-input-group--space">
            <input
                type="text"
                placeholder="..."
                className="search-input search-input--docs"
            />
        </div>
    );
};

export const VirtualPathComponent = () => {
    return (
        <div className="search-input-group search-input-group--final">
            <FontIcon
                iconName="FolderList"
                className="fabric-ui-icon"
            />
            <input
                type="text"
                placeholder="/"
                className="search-input search-input--docs"
            />

        </div>
    );
};
