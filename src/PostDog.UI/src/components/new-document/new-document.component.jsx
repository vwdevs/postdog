import React from 'react';
import './new-document.styles.scss';
import {
    DescriptionComponent,
    FromSearchComponent,
    TagSearchComponent,
    VirtualPathComponent,
} from './new-docs-search.component';
import DragNDropComponent from './dragndrop.component';
import { DefaultButtonComponent } from '../../shared/components/buttons/buttons.component';

const submitForm = e => {
    e.preventDefault();
};

const NewDocumentComponent = ({ handleClose }) => {
    return (
        <div className="new-document-modal">
            <div
                className="new-document-modal__backdrop"
                onClick={handleClose}
            />
            <div className="new-document-modal__body">
                <div className="document-modal__close" onClick={handleClose} />
                <h4>Create New File</h4>

                <form
                    onSubmit={submitForm}
                    className="new-document-form"
                    encType="multipart/form-data"
                >
                    <label htmlFor="from-search-id">FROM</label>
                    <FromSearchComponent id="from-search-id" />

                    <label htmlFor="enter-tags-id">TAGS</label>
                    <TagSearchComponent id="enter-tags-id" />

                    <label htmlFor="description-id">DESCRIPTION</label>
                    <DescriptionComponent id="description-id" />

                    <label htmlFor="virtualpath-id">PATH</label>
                    <VirtualPathComponent id="virtualpath-id" />

                    <DragNDropComponent />

                    <div className="new-document-form__button-list">
                        <DefaultButtonComponent handleClick={submitForm}>
                            SUBMIT
                        </DefaultButtonComponent>
                        <DefaultButtonComponent handleClick={handleClose}>
                            CANCEL
                        </DefaultButtonComponent>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default NewDocumentComponent;
