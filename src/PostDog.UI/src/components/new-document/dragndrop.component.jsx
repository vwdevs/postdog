import { FontIcon } from 'office-ui-fabric-react';
import React, { useState } from 'react';

const FileEntryComponent = ({ fileName, handleClick }) => {
    return (
        <div className="file-entry">
            <div className="file-entry__name">{fileName}</div>
            <div className="file-entry__progressbar" />
            <div className="file-entry__cancel" onClick={handleClick} />
        </div>
    );
};

const DragNDropComponent = () => {
    const fileInputRef = React.createRef();
    let [highlight, isHighlighted] = useState(false);
    let [fileList, setFileList] = useState([]);
    let [isDropped, setDropped] = useState(false);

    const openFileDialog = () => {
        fileInputRef.current.click();
    };

    const onInputChange = event => {
        const files = event.target.files;
        if (files) {
            readFiles(files);
            setFileList(fileListToArray(files));
        }
        setDropped(true);
    };

    const readFiles = fileList => {
        for (let i = 0; i < fileList.length; i++) {
            const reader = new FileReader();
            reader.readAsArrayBuffer(fileList[i]);

            reader.onprogress = function(data) {
                if (data.lengthComputable) {
                    let progress = parseInt(
                        (data.loaded / data.total) * 100,
                        10
                    );
                    console.log(progress);
                }
            };
        }
    };

    const onDragOver = evt => {
        evt.preventDefault();
        evt.stopPropagation();
        isHighlighted(true);
    };

    const onDragLeave = () => {
        isHighlighted(false);
    };

    const onDrop = event => {
        event.preventDefault();
        event.stopPropagation();
        const files = event.dataTransfer.files;
        if (files) setFileList(fileListToArray(files));

        isHighlighted(true);
    };

    const fileListToArray = list => {
        const array = [];
        for (let i = 0; i < list.length; i++) {
            array.push(list.item(i));
        }
        return array;
    };

    const removeFileEntry = fileIndex => {
        setFileList([
            ...fileList.filter(file => fileList.indexOf(file) !== fileIndex),
        ]);
    };

    return (
        <div
            className={`drag-n-drop ${isDropped ? 'hasEntry' : ''}`}
            onDragOver={onDragOver}
            onDragLeave={onDragLeave}
            onDrop={onDrop}
            onClick={openFileDialog}
        >
            <input
                ref={fileInputRef}
                type="file"
                multiple
                onChange={onInputChange}
            />

            {isDropped ? (
                fileList.map((file, index) => (
                    <FileEntryComponent
                        key={index}
                        fileName={file.name}
                        handleClick={() => removeFileEntry(index)}
                    />
                ))
            ) : (
                <FontIcon
                    iconName="CloudAdd"
                    className={`drag-n-drop__zone ${
                        highlight ? 'highlight' : ''
                    }`}
                />
            )}
        </div>
    );
};

export default DragNDropComponent;
