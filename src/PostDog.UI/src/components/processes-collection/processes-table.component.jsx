import React from 'react';

const ProcessesTableComponent = ({ processedDocs }) => {
    return (
        <table className="processes__table">
            <colgroup>
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th>File</th>
                    <th>Progress</th>
                    <th>Updated</th>
                    <th>Id</th>
                </tr>
            </thead>
            <tbody>
                {processedDocs.map(doc => (
                    <tr key={doc.id}>
                        <td>{doc.filename}</td>
                        <td>
                            <progress
                                max={100}
                                value={doc.progress}
                                className="process__progress"
                            >
                                ASFD
                            </progress>
                        </td>
                        <td>{doc.updated}</td>
                        <td>{doc.id}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default ProcessesTableComponent;
