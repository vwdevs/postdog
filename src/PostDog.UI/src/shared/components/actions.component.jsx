import { FontIcon } from "office-ui-fabric-react/lib/Icon";
import React from "react";

export const AddDocumentActionComponent = ({handleClick}) => {
  return (
    <div className="action" onClick={handleClick}>
      <FontIcon iconName="OpenFile" className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const HelpActionComponent = () => {
  return (
    <div className='action'>
      <FontIcon iconName='Help' className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const ProcessActionComponent = () => {
  return (
    <div className='action'>
      <FontIcon iconName='LightningBolt' className="fabric-ui-icon fabric-ui-icon--mini" />
    </div>
  );
};

export const FolderActionComponent = () => {
    return (
        <div className='action'>
            <FontIcon iconName='FolderList' className="fabric-ui-icon fabric-ui-icon--mini" />
        </div>
    );
}