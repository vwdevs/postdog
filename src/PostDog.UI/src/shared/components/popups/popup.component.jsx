import React, { useState } from 'react';
import './popup.styles.scss';

export const TagPopupComponent = ({ tags, handleClose, filterDocuments }) => {
    let [filteredTags, setTags] = useState(tags);
    let [activeTags, setActiveTags] = useState([]);

    const filterTags = event => {
        setTags(
            tags.filter(tag =>
                tag.toLowerCase().includes(event.target.value.toLowerCase())
            )
        );
    };

    const toggleTag = value => {
        if (activeTags.includes(value)) {
            setActiveTags([...activeTags.filter(tag => tag !== value)]);
        } else {
            setActiveTags([...activeTags, value]);
        }

        // if (activeTags.length > 0) {
        //     filterDocuments(activeTags);
        // } else {
        //     filterDocuments(tags);
        // }
    };

    return (
        <div>
            <div className="popup__backdrop" onClick={handleClose} />
            <div className="popup">
                <div className="search-input-group">
                    <input
                        type="text"
                        className="search-input search-input--docs"
                        onChange={filterTags}
                        placeholder="Search here..."
                    />
                </div>

                {filteredTags.map((tag, index) => (
                    <div
                        key={index}
                        className={
                            activeTags.includes(tag) ? 'tag is-active' : 'tag'
                        }
                        onClick={() => toggleTag(tag, index)}
                    >
                        {tag}
                    </div>
                ))}
            </div>
        </div>
    );
};
