const DEFAULT = 1;
const SUCCESS = 2;
const WARNING = 3;
const DANGER = 4;
const ToastyTypes = {DEFAULT, SUCCESS, WARNING, DANGER};

export default ToastyTypes;