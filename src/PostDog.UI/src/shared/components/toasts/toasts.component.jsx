import React from 'react';
import './toasts.styles.scss';
import ToastyTypes from './toasty.types';

const DefaultToastComponent = ({ handleClose }) => {
    return (
        <div className="toast-flex-container toast-flex-container--default">
            <div className="toast__highlight toast__highlight--default" />
            <div className="toast__close" onClick={handleClose} />
            <div className="toast__body">Default Message</div>
        </div>
    );
};

const SuccessToastComponent = ({ handleClose, file }) => {
    return (
        <div className="toast-flex-container toast-flex-container--success">
            <div className="toast__highlight toast__highlight--success" />
            <div className="toast__close" onClick={handleClose} />
            <div className="toast__body">
                Successfully deleted <p>{file}</p>
            </div>
        </div>
    );
};

const WarningToastComponent = ({ handleClose, file }) => {
    return (
        <div className="toast-flex-container toast-flex-container--warning">
            <div className="toast__highlight toast__highlight--warning" />
            <div className="toast__close" onClick={handleClose} />
            <div className="toast__body">
                File could not be located<p>{file}</p>
            </div>
        </div>
    );
};

const DangerToastComponent = ({ handleClose, file }) => {
    return (
        <div className="toast-flex-container toast-flex-container--danger">
            <div className="toast__highlight toast__highlight--danger" />
            <div className="toast__close" onClick={handleClose} />
            <div className="toast__body">
                Could not delete <p>{file}</p>
            </div>
        </div>
    );
};

function selectToastyByType({ type, file }, idx, handleClose) {
    switch (type) {
        case ToastyTypes.DEFAULT:
            return (
                <DefaultToastComponent
                    key={idx}
                    file={file}
                    handleClose={() => handleClose(idx)}
                />
            );
        case ToastyTypes.SUCCESS:
            return (
                <SuccessToastComponent
                    key={idx}
                    file={file}
                    handleClose={() => handleClose(idx)}
                />
            );
        case ToastyTypes.WARNING:
            return (
                <WarningToastComponent
                    key={idx}
                    file={file}
                    handleClose={() => handleClose(idx)}
                />
            );
        case ToastyTypes.DANGER:
            return (
                <DangerToastComponent
                    key={idx}
                    file={file}
                    handleClose={() => handleClose(idx)}
                />
            );
        default:
            break;
    }
}

export const ToastsContainerComponent = ({ toasties, filterToasties }) => {
    return (
        <div className="toast-container">
            {toasties.map((toasty, index) =>
                selectToastyByType(toasty, index, filterToasties)
            )}
        </div>
    );
};
