import React from "react";
import './nav-button.styles.scss';
import { FontIcon } from 'office-ui-fabric-react/lib/Icon';

const NavButtonComponent = ({ icon, desc }) => {
    return (
        <div className="nav-button">
            <div className="nav-button__icon">
                <FontIcon iconName={icon} className="fabric-ui-icon fabric-ui-icon--nav" />
            </div>
            <div className="nav-button__description">{desc.toUpperCase()}</div>
        </div>
    );
};

export default NavButtonComponent;
