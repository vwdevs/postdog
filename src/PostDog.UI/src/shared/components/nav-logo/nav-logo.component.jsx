import React from 'react';
import './nav-logo.styles.scss';

const NavLogoComponent = () => {
    return (
        <div className='nav-area'>
            <div className='nav-logo'>
                LOGO
            </div>
        </div>
    );
};

export default NavLogoComponent;