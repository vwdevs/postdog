import React from "react";
import './search.styles.scss'

const FullSearchComponent = ({placeHolder, onChange}) => {
  return (
    <div className='search-input-group'>
      <input type="text" placeholder={placeHolder} className='search-input' onChange={onChange}/>
    </div>
  );
};

export default FullSearchComponent;