import React from 'react';

export const DefaultButtonComponent = props => {
    return <button className="button" onClick={props.handleClick}>{props.children}</button>;
};
