import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Suspense, lazy } from 'react';
import UserPage from '../../pages/user/user.component';
import SettingsPage from '../../pages/settings/settings.component';

const DocumentsPages = lazy(() =>
    import('../../pages/documents/documents.component')
);
const ProcessPage = lazy(() => import('../../pages/process/process.component'));

const Routes = () => (
    <Switch>
        <Suspense fallback="..loading">
            <Route exact path="/documents" component={DocumentsPages} />
            <Route exact path="/processes" component={ProcessPage} />
            <Route exact path="/user" component={UserPage} />
            <Route exact path="/settings" component={SettingsPage} />
        </Suspense>
        {/*<Route component={PageError} />*/}
    </Switch>
);

export default Routes;
