import React from 'react';
import './grid.styles.scss';
import Routes from '../../constants/routes';
import NavLogoComponent from '../../components/nav-logo/nav-logo.component';
import { NavLink } from 'react-router-dom';
import NavButtonComponent from '../../components/nav-button/nav-button.component';

const GridContainerComponent = () => {
    return (
        <div className="grid-container">
            <nav className="sidenav-grid">
                <NavLogoComponent />
                <NavLink
                    to="/documents"
                    className="nav-button"
                    activeClassName="nav-button--active"
                >
                    <NavButtonComponent icon="Script" desc="Documents" />
                </NavLink>
                <NavLink
                    to="/processes"
                    className="nav-button"
                    activeClassName="nav-button--active"
                >
                    <NavButtonComponent
                        icon="Processing"
                        desc="Processes"
                    />
                </NavLink>
                <NavLink
                    to="/user"
                    className="nav-button"
                    activeClassName="nav-button--active"
                >
                    <NavButtonComponent icon="Contact" desc="User" />
                </NavLink>
                <NavLink
                    to="/settings"
                    className="nav-button"
                    activeClassName="nav-button--active"
                >
                    <NavButtonComponent icon="Settings" desc="Settings" />
                </NavLink>
            </nav>

            <main className="main-grid">
                <Routes />
            </main>
        </div>
    );
};

export default GridContainerComponent;
