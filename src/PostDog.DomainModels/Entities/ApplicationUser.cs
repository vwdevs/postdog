using Microsoft.AspNetCore.Identity;

namespace PostDog.DomainModels.Entities
{
    public sealed class ApplicationUser : IdentityUser<int>
    {
    }
}