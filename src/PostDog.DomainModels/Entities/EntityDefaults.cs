namespace PostDog.DomainModels.Entities
{
    public static class EntityDefaults
    {
        public const int MaxCharLength = 256;
        public const int MaxStringLength = 1024;
        public const int MaxPathLength = 4096;
        public const string DefaultVirtualPath = "/";
        public const int DefaultPageCount = 1;
        public const int MaxIndexLength = 256;
        public const string DefaultDateValue = "GETDATE()";
    }
}