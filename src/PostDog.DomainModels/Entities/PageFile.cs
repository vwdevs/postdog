namespace PostDog.DomainModels.Entities
{
    public class PageFile
    {
        public int Id { get; set; }
        public int PageIndex { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public int SourceFileId { get; set; }
        public SourceFile SourceFile { get; set; }
    }
}