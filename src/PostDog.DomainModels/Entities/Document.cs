using System;
using System.Collections.Generic;

namespace PostDog.DomainModels.Entities
{
    public class Document
    {
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string VirtualPath { get; set; }
        public Guid DocumentGuid { get; set; }
        public string Slug { get; set; }

        public List<SourceFile> SourceFiles { get; set; }
        public List<DocumentTag> DocumentTags { get; set; }
    }
}