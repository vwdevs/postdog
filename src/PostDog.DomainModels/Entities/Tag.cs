using System.Collections.Generic;

namespace PostDog.DomainModels.Entities
{
    public class Tag
    {
        public int Id { get; set; }
        public string Label { get; set; }

        public List<DocumentTag> DocumentTags { get; set; }
    }
}