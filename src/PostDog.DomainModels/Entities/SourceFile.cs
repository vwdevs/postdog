using System.Collections.Generic;

namespace PostDog.DomainModels.Entities
{
    public class SourceFile
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int PageCount { get; set; }

        public List<PageFile> Pages { get; set; }

        public int DocumentId { get; set; }
        public Document Document { get; set; }

        public FileConversion FileConversion { get; set; }
    }
}