using System;
using PostDog.Common.Globals;

namespace PostDog.DomainModels.Entities
{
    public class FileConversion
    {
        public int Id { get; set; }
        public ConversionState State { get; set; }
        public DateTime Updated { get; set; }

        public int SourceFileId { get; set; }
        public SourceFile SourceFile { get; set; }
    }
}