namespace PostDog.Common.Globals
{
    public enum ConversionState
    {
        Started = 1,
        Failed = 2,
        Completed = 3
    }
}