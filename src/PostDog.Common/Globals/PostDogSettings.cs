using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostDog.Common.Globals
{
    public class PostDogSettings
    {
        public bool IsSetup { get; set; }
        public string Token { get; set; }
        public byte[] EncodedToken => !string.IsNullOrEmpty(Token) ? Encoding.ASCII.GetBytes(Token) : new[] {byte.MinValue};

        [Required] public DatabaseSettings Database { get; set; }
    }
}