using System.ComponentModel.DataAnnotations;

namespace PostDog.Common.Globals
{
    public class DatabaseSettings
    {
        public DatabaseType Type { get; set; }

        [Required] public string ConnectionString { get; set; }
    }
}