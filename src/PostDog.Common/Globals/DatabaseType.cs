namespace PostDog.Common.Globals
{
    public enum DatabaseType
    {
        Undefined = 0,
        Sqlite = 1,
        MySql = 2,
        PostgreSql = 3,
        SqlServer = 4
    }
}