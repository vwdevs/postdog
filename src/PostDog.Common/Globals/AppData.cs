using System.IO;

namespace PostDog.Common.Globals
{
    public static class AppData
    {
        public const string AppPath = "appdata";
        public static readonly string SettingsFile = Path.Join(AppPath,"postdog.json");
    }
}