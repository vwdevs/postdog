using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using PostDog.Common.Globals;

namespace PostDog.Services.Services
{
    public class ConfigService
    {
        public void InitializeAppData(PostDogSettings settings)
        {
            try
            {
                Directory.CreateDirectory(AppData.AppPath);
                if (!File.Exists(AppData.SettingsFile)) Task.FromResult(UpdateAppData(settings));
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task UpdateAppData(PostDogSettings settings)
        {
            try
            {
                var content = JsonSerializer.Serialize(settings, new JsonSerializerOptions {WriteIndented = true});
                await File.WriteAllTextAsync(AppData.SettingsFile, content);
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}