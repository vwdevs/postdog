using System;
using System.Collections.Generic;
using System.Linq;
using PostDog.DataSource;
using PostDog.Protocol.Models.Documents;

namespace PostDog.Services.Services
{
    public class DocumentService
    {
        private readonly IGenericContext _context;

        public DocumentService(IGenericContext context)
        {
            _context = context;
        }

        public IEnumerable<DocumentResult> DocumentSearch(DocumentRequest dto)
        {
            return new[] {new DocumentResult()};
        }

        public DocumentResponse GetPaginatedDocuments(int skip, int take)
        {
            var result = _context
                .Documents
                .Skip(skip)
                .Take(take)
                .Distinct()
                .Select(x => new DocumentResult
                {
                    Description = x.Description,
                    Sender = x.Sender,
                    CreatedDate = x.CreatedDate,
                    VirtualPath = x.VirtualPath
                });

            return new DocumentResponse
            {
                PageCount = (int) Math.Ceiling((decimal) result.Count() / take),
                Data = result
            };
        }
    }
}