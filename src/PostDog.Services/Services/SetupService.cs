using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PostDog.Common.Globals;
using PostDog.Protocol.Models.Setup;

namespace PostDog.Services.Services
{
    public class SetupService
    {
        private readonly ConfigService _configService;
        private readonly MigrationService _migrationService;
        private readonly IOptions<PostDogSettings> _options;

        public SetupService(ConfigService configService, MigrationService migrationService, IOptions<PostDogSettings> options)
        {
            _configService = configService;
            _migrationService = migrationService;
            _options = options;
        }

        public async Task SetupPostdog(SetupRequest setupRequest)
        {
            await _configService.UpdateAppData(CreateSettings(setupRequest));
            await _migrationService.RunMigration(CreateSettings(setupRequest));
        }

        private PostDogSettings CreateSettings(SetupRequest setupRequest)
        {
            return new PostDogSettings
            {
                IsSetup = true,
                Token = _options.Value.Token,
                Database = new DatabaseSettings
                {
                    Type = setupRequest.DatabaseType,
                    ConnectionString = setupRequest.ConnectionString
                }
            };
        }
    }
}
