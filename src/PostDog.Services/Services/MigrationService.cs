using System.Threading;
using System.Threading.Tasks;
using PostDog.Common.Globals;
using PostDog.DataSource.Contexts;

namespace PostDog.Services.Services
{
    public class MigrationService
    {
        private readonly IContextFactory _contextFactory;

        public MigrationService(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        
        public async Task RunMigration(PostDogSettings settings)
        {
            using (var context = _contextFactory.CreateContext(settings))
            {
                await context.RunMigrationsAsync(CancellationToken.None);
            }
        }
    }
}