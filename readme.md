# Requirements for development
* Node.Js
* .Net core 3.1
* Rabbit MQ
* IDE of your choice

# Setup
Change directory into PostDog.Web and type npm install.
Then type in the main directory PostDog dotnet run.

If you want to access the cli change into PostDog.Cli and type dotnet run.

To run via docker cange into the main directory and run docker-compose -f docker-compose.yml -f docker-compose.dev.yml up.

